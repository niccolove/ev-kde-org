The seventh edition of the <a href="https://lakademy.kde.org/">LaKademy - the Latin American Akademy</a> took place from 14th to 17th November 2019 at the Superintendency of Information Technology of the Federal University of Bahia in Salvador-Bahia, Brazil. Contributors and members of the community gathered for four days of hacking sessions, promo meetings, and all sort of KDE-related things. 

LaKademy started with a round of presentations of projects for new and potential contributors. Later, we exchanged ideas about different projects and initiatives and participated in different hacking sessions where contributors worked on projects.  

We worked on KDE projects like LaKademy’s website, KDE Brazil's migration from Phabricator to GitLab, <a href="https://cantor.kde.org/">Cantor</a>; porting <a href="">Sprat</a>, an editor of papers drafts in English, to Qt5; ROCS; fixes for redundant code; bugs plaguing Khipu, etc.

As always, we held our traditional promo meeting. During the meeting, we discussed future actions for KDE in Latin America and strategies to make our community stronger. The main decisions were to organize small, distributed events in the cities and to organize KDE's presence at <a href="https://flisol.info/">FLISoL</a> and <a href="https://www.softwarefreedomday.org/">Software Freedom Day</a>. We also discussed creating a KDE Brazil Promo group on Telegram to coordinate activities in Brazil.

It was a great event where not only veteran members guided new members, but where we also worked passionately to create better softwares for everyone.
