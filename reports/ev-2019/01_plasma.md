The KDE Plasma and Usability teams converged on the beautiful Spanish city of Valencia in June for a combined development sprint. The teams admired Valencia's medieval architecture and stayed up until midnight eating sumptuous Mediterranean food. But of course, the real purpose was work! 

We camped out in the offices of the <a href="https://slimbook.es">Slimbook</a> company, which were generously made available for the sprint. The aim was not only to hack on Plasma and the <a href="https://community.kde.org/Goals/Usability_%26_Productivity">Usability & Productivity goal</a>, but also to benefit from the cross-pollination opportunities provided by hosting both sprints at the same time and place.

The result was a huge amount of work done on <a href="https://kde.org/plasma-desktop">Plasma</a>, <a href="https://userbase.kde.org/KWin">KWin</a>, <a href="https://kde.org/applications/system/org.kde.dolphin">Dolphin</a>, <a href="https://kde.org/applications/utilities/org.kde.spectacle">Spectacle</a>, and many other bits of KDE software.

Present for the Plasma sprint were Kai Uwe Broulik, David Edmundson, Nicolas Fella, Eike Hein, Roman Gilg, Aleix Pol Gonzalez, Marco Martin, and Bhushan Shah. They had quite a busy agenda:


+ Plasma 5.16's new notification system received a great deal of polish
+ Fixed a ton of bugs in the Plasma Browser Integration
+ Rewrote the widget positioning code for the desktop, making it much more robust, future-proof, and usable on touch:

<center style="margin: 1ex">
<iframe width="560" height="315" src="https://www.youtube.com/embed/sk2ElfY-iBM" allowfullscreen></iframe>
</center>

+ Started work on making the Task Manager understand window stacking order, which will allow it to implement new interaction modes for grouped windows (e.g. bring forward the last-used window when clicked)
+ Worked on architecture improvements for the Task Manager to unify its different presentation modes and improve code robustness
+ Worked on a variety of architecture improvements for KWin to make it more future-proof, which, among other things, will improve multi-screen handling
+ Improved the user interface for the System Tray's settings window
+ Added calculator and unit conversion functionality to Kickoff and Application Dashboard


In addition to making technical progress, the Plasma and Usability teams got together to discuss a number of long-standing Plasma issues, and figure out how to resolve them:

<strong>We wanted to make it easier to test a custom-compiled version of Plasma.</strong> To do so, we implemented changes that allow you to integrate your custom-compiled Plasma into SDDM by running a single command, after which you can log into it normally. For more information, see <a href="https://pointieststick.com/2019/06/25/an-easier-way-to-test-plasma/">this article</a>.

We thought it would be a good idea to make it more obvious and discoverable that Plasma is made up of widgets, and show how they are configured. To do this, we decided to create a new "global edit mode" that's triggerable from within <i>System Settings</i>, as this is where new users generally expect everything to be configured. In this global edit mode, all widgets become visibly configurable, editable, removable, etc. We also wanted to make it easy to change the wallpaper in this mode. With all that done, we were able to remove the Desktop Toolbox as it existed at the time.

There was a need to unify the disparate scaling methods, so we decided to visually connect the scale factor chooser with the "Force Fonts DPI" setting, since the former actually affects the latter, but not the other way around. This should make it clear that the scaling slider is the primary way to scale the screen, and the "Force Fonts DPI" control is nothing more than a way to tweak things further.

We needed Plasma to respect the system-wide scale factor on X11, so we came up with a path forward and a plan for getting it done!

We planned out how to add power actions to the lock screen. We concluded that not only did this make sense, but it would be necessary for Plasma Mobile anyway. In a multi-user environment, the user will have to enter an admin password to shut down or restart the machine when other users are also logged in.

Over in the Usability & Productivity room, we had Méven Car, Albert Astals Cid, Noah Davis, Filip Fila, Nate Graham, and David Redondo. The agenda was similarly jam-packed, and included the following:


+ We ported Spectacle to use KGlobalAccel and away from KHotKeys, made the quit-after-copy feature finally work, and added support for drawing annotations on newly-taken screenshots
+ We implemented user-configurable sort ordering for wallpaper slideshows
+ Dolphin received human-readable sort order text and an auto-play-on-hover feature for media files
+ We added inline name feedback when creating new files or folders
+ Users can optionally close windows in the Present Windows effect with a middle-click
+ Many user interface improvements were made to the Purpose framework, which implements sharing support in many apps (Dolphin, Spectacle, Okular, Gwenview, and so on)
+ We started working on improving the default selection of pictures available for user account avatars
+ Initial work was done on a new "Recently used" feature for Dolphin and the file dialogs that pulls its data from a single consistent location and actually notice everything


We also came to some significant conclusions related to higher-level goals. For example, we planned to pay for professional user research to generate new "personas" and target user groups that represent the people using our software. We would use these personas as the basis for professional usability testing for Plasma, Dolphin, Gwenview, Okular, and other components of a basic desktop.

Additionally, we discussed how we could add release notes data to our apps' AppStream data, so that it shows up in software center apps like Discover. The big blocker was getting the required translations added to the tarball. We started a dialogue with AppStream maintainer Matthias Klumpp regarding a new feature to pull translations from a remote location, which would support our workflow.

Finally, VDG member Noah Davis dug deep into Breeze to work on visual consistency improvements related to selection highlights. Given his familiarity with the code, he was well on his way to becoming the next Breeze maintainer!

All in all, it was a very productive week. KDE Plasma and apps are in a great place right thanks to the work we carried out, and the team's effort to further improve things reached users in the next versions.
