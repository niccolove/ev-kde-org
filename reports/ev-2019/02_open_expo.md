<a href="https://openexpoeurope.com/">OpenExpo</a> is an event aimed at businesses and the public sector. Top topics usually revolve around cloud computing, big and open data, IoT, and, of late, blockchain technologies. 2019 was its 6th edition and was held on the 20th of June in <a href="http://www.lanavemadrid.com/">"La Nave"</a> on the outskirts of Madrid.

Organizers told us that 2800 visitors attended the 2019 event, there were about 120 speakers and 70 exhibitors with booths. From what we can garner, most visitors were representatives of public institutions, consulting companies, and software-producing companies, with a prevalence of firms working in the area of cloud computing.

<h2>The Booth</h2>

KDE's booth was right next to the entrance, on the right as you went in, in an area called the "Innovation & Community Village". We were one of five exhibitors in the area. On our right was the <a href="https://fsfe.org/">FSFE</a>. I happened to know one of the people staffing, which was nice.

Behind us was a father-and-son outfit showing off 3D printers. Apart from owning a shop, they apparently run courses in their neighborhood and that is what earned them a spot in the "Community Village". 

Then there were some people with a <a href="https://onemove.es/">DIY go-kart/scooter/tricycle thingy</a> (?). They opened a big colorful box full of interesting looking pieces, didn't do anything with them, and then left.

Finally, on the other side of our table was <a href="http://www.isardvdi.com/">a company/community that virtualized desktops in the browser</a>. Interesting stuff.

There were six tables and it was first come, first served, and I was first, so I picked a front-facing table.  Each table was 180 long by 80 cms, which is big compared to what we often get in other events and gave us plenty of space to set up our things. There was a space for our banner in a corner, as you can see in the photograph. We added a screen on a stand behind us to run videos on Plasma Mobile, Plasma, Kirigami and Applications on a loop. You can see the screen in the background of the photo.

On the table we laid out the following items:

+ a Nexus 5x running Plasma Mobile (thanks Aleix Pol)
+ a Raspberry Pi with touchscreen and Plamo running upon Yocto, courtesy of Volker Krause
+ a Pinebook 14'' running Plasma (again courtesy of Aleix Pol)
+ a KDE Slimbook 2, on loan from Slimbook themselves who were also at the event.

Each device was clearly labelled as to what it was with a printed piece of paper which included a URL and QR which the visitors could scan to find out more.

[image of sheets]

We also had 100 stickers, 50 Katie stickers using a phone and with the Plasma Mobile URL, and 50 Konqi stickers with KDE.org URL. The Konqi stickers ran out first.

[Images of stickers]

The aim of the spread was three-fold: first, we wanted to show people "shopping" for software how Plasma and KDE applications were "end-user ready". Secondly, we intended to show how Plasma was light and could work on a wide variety of devices, including on devices used in embedded systems, like the Raspberry PI; on low-powered, ARM-based netbooks (the Pinebook); and as a potential mobile environment (the Nexus 5X). Finally, we wanted to demonstrate how applications, thanks to Kirigami, could adapt to different hardware and screen configurations

The overarching aim was to see if we could convince administrators of large deployments (for example, schools) that Plasma and KDE Applications would be a good choice for their users. We were also looking to convince companies that KDE has good solutions for the development of graphical applications, and seeking contributors and sponsors for KDE.

<h2>What I did</h2>

To attract and engage visitors I used several tactics I had used in the past and that seems to work well. I stood outside the booth and approached visitors that showed interest in our spread.

I found out where the visitors were coming from and adapted my spiel to that. I demoed Plasma on laptops for administrators of large deployments, showing off features and with demonstrations of how fast and snappy, it was, even on low-spec hardware.

I showed the proof of concepts of Plasma Mobile on Yocto (on the Raspberry Pi) and on postmarketOS (on the Nexus 5) to managers of companies that developed for several platforms. They could check for themselves how Kirigami could let them create cross-platform applications, including for Android (I had my phone on hand for this) and how it would allow them to create applications that would adapt to different sizes of screens.

At the end of each demonstration, I encouraged visitors to scan the QRs so they could leave with more information they could research for themselves.

The thing that most attracted visitors' attention was the Pinebook when they read it cost 99 USD. That sparked interest in the underlying hardware and what software would run on an underpowered device. A lot of people also picked up the SBC for some reason. The Pine64 I had brought along was only there to show what kind of hardware was in the Pinebook, but it seems that naked electronics are inherently fascinating for visitors to these kinds of events. I suppose it is the inner-geek we all have inside.

After the Pinebook, the most popular devices where the phone and the Raspberry Pi with its touchscreen. A lot of visitors asked if the phone was already for sale, thinking that a pure GNU + Linux phone was already a thing and they had somehow missed it. Even though I had to burst their bubble, they were satisfied that at least some progress was going on, both in the realms of mobile phones and vehicle infotainment systems.

<h2>Visitors</h2>

The scanning application provided by the organizers of the event was very useful and I scanned 54 people in total, but, of course, I talked to more than that. By my calculations, I talked one-to-one to about 50% more than what I scanned, which puts the number of people I interacted with in depth between 75 and 80. There were also four or five times when, while delivering my spiel, a small crowd congregated of 5 to 10 people, so a conservative number of the total people I talked to would be around 100.

Many of them were system administrators specialized in the area of cloud computing, one of the main topics of the event. Others managed large networks of end-user machines for schools, libraries and other public institutions. There were also plenty of CEOs, CTOs and other C*Os, both attending for the talks and "shopping" for new open source development software. These are the people that found things like Kirigami interesting.

There were Linux desktop end users in the mix too. Many of them did not use Plasma (a few did) and they were under the impression that Plasma was heavy. The Pinebook disproved that, but this, that KDE software is bloated, is something, we have seen before and we clearly must continue to work towards dispelling this notion.

I tried to make sure that visitors to the booth walked away with something to remember us by: Stickers with KDE.org URLs on them until they run out; my card, in case they needed more information; or at the very least the links to more information in the browsers on their phones, as I encouraged people to scan the QRs associated with each item on the table.

<h2>Mission(s) Accomplished?</h2>

One of the things I set out to do was to generate some publicity for KDE in the mainstream media since it was announced that journalists from some big Spanish newspapers, radios and TVs would be there. Unfortunately, I did not see them.

However, I was not disappointed with the day, since we achieved other things on the list. We made contacts within several Madrilian institutions, like the leaders of the MAX Linux distribution, deployed in many Madrilian schools. They were currently using MATE for their desktop, but after reviewing our spread, they said they would give Plasma a try.

Continuing with public institutions, we also talked to the people who managed the libraries in Alcorcón, sysadmins from the Congreso de los Diputados and the Ministerio de Economía and Hacienda, and developers from Correos, the Spanish post office. There were representatives from several universities, both students and professors. All visitors were impressed by Plasma's feature set, performance and flexibility and were excited about trying it out at work and home.

The students from the <a href="https://librelabucm.org/">LibreLabUCM</a> of the Universidad Complutense de Madrid later wrote to me and asked how they could contribute. They were especially interested in contributing to Plasma Mobile.

We had a mixed bag when it came to visitors from private enterprises. There were both coders and managers among the people who came to the booth, as well as freelance consultants. Many of the managers, that included CEOs, CTOs and product managers, and all the consultants seemed to be "shopping" for FLOSS to boost productivity (the former) or to add to their portfolio (the latter). Although they were mainly after infrastructure-like software, like cloud management systems, they would often become interested when I demoed Kirigami-based software and how it was possible to create good-looking, graphical applications for most platforms and that would adapt to different screen sizes and shapes.

From the bigger, more recognizable company's, we had visitors from IBM, Oracle, BT, Atos, Allfunds Bank and Wacom. From smaller, Spanish joints we met people from VASS, Zylk, Zendata and Certelia.

<h2>Lessons learnt</h2>

The first lesson I learnt was not to try and do this alone again. Over twelve hours standing and greeting visitors is not good for an unfit, overweight 53-year-old. Being alone also meant I had to rely on the kindness of the people in the FSFE booth when I had to go foraging for water and food or for when I needed a bathroom break – thanks, Pablo and Erik!

But, seriously, next time we should show some "naked" electronics. This fascinates attendees for some reason. We should maybe acquire the RISC V board we showed in FOSDEM. These kinds of things attract visitors like magnets.

I noticed many visitors looking over the booth from afar, trying to figure out who we were before approaching. As the roll up banner was to one side, it was not always obvious that it was associated with us. A solution would be to always make sure we have a tablecloth or prominent flag with our logo, name and URL handy. We had both at the booth at FOSDEM and I'm pretty sure that helped.

The stickers run out rather quickly. By two o'clock there were none left. It wasn't a big issue, because the event wasn't the type that attracted merch-scavengers and most people were more interested in what we had on display than in stockpiling goodies. But it would still have been nice to have had more. Related: I also learnt that vinyl die-cut stickers are expensive: 60 euros for 100 stickers.

Continuing in the area of printed merch, maybe make attractive flyers with colored pictures, snappy explanatory bites, shortened URLs and no marketing-speak relevant to what is on show at the booth: not everybody has QR scanning software on their phones and a printed guide explaining what we were showing at the booth would've helped and served as a reminder if attendees could've taken them with them.

<h2>Was it worth it?</h2>

Yes. We made a lot of contacts in companies and institutions it would have been difficult to get in touch with any other way. We also heard about the problems they have and we can use that to see what solutions we can offer. Both things will ultimately help grow the number of companies that use KDE technologies (like Kirigami) in their products, as well as help us convince institutions to deploy our software (like Plasma and Applications) for their users.
