---
title: 2019 KDE e.V. Report
summary: KDE e.V. is happy to inform you that we have just published the latest report about the activities of our association in 2018. KDE's yearly report gives a comprehensive overview of all that has happened during 2018. It covers the progress we have made with KDE's Plasma and applications as well as results from community sprints, conferences, and external events the KDE community has participated in worldwide.

layout: report

menu:
  - title: Home
    anchor: home
    home: true
  - title: Welcome Message
    heading: Welcome to KDE's Annual Report 2019
    image: images/logos/kde-logo.png
    author: Aleix Pol
    include: 00_welcome-message.html
  - title: Featured article
    heading: Featured article - What is the KDE Free Qt Foundation and What does it do
    image: images/logos/kde-logo.png
    author: Paul Brown
    include: 00_featured-article.html
  - title: Supported Activities
    items:
      - title: Developer Sprints and conferences
        heading: Supported Activities &#8210; Developer Sprints and Conferences
        items:
          - include: 01_gsoc.html
            heading: Google Summer of Code
            image: images/logos/gsoc_logo.png
            author: By Valorie Zimmerman
          - include: 01_kdeconnect.html
            heading: KDE Connect Sprint
            image: images/logos/kdeconnect.png
            author: By Simon Redman and Matthijs Tijink
          - include: 01_krita.html
            image: images/logos/krita.png
            author: By the Krita Team
            heading: The Krita Sprint
          - include: 01_akademy.html
            heading: Akademy 2019
            author: By Paul Brown and Ivana Devcic
            image: images/logos/akademy_logo.png
          - include: 01_usuability_and_productivity.html
            heading: Usability & Productivity Sprint
            author: By David Redondo and Nicolas Fella
            image: images/logos/goals.png
          - include: 01_kf6.html
            author: Compiled from notes made by Christoph Cullmann, David Redondo, Kai Uwe, Kevin Ottens, Marco Martin, Nicolas Fella and Volker Krause
            heading: KF6 Sprint
            image: images/logos/kde-logo.png
          - include: 01_privacy.html
            author: By the Privacy Team
            heading: Privacy Goal Sprint
            image: images/logos/goals.png
          - include: 01_plasma_mobile.html
            author: By the Plasma Mobile Team
            heading: Plasma Mobile Sprint
            image: images/logos/kdeconnect.png
          - include: 01_plasma.md
            author: By the Plasma Team
            heading: Plasma Sprint
            image: images/logos/plasma.png
          - include: 01_onboarding_sprint.md
            author: By Neofytos Kolokotronis 
            heading: Onboarding Sprint
            image: images/logos/goals.png
          - include: 01_las.md
            author: Compiled from blogs by Akhil Gangadharan, Amit Sagtani, Jonathan Riddel, and Kenny Coyle
            heading: Linux App Summit
            image: images/logos/kde-logo.png
          - include: 01_lakademy.md
            author: Compiled from the blogs of Aracele Torres, Caio Tonetti, Filipe Saraiva and Sandro Andrade
            heading: LaKademy
            image: images/logos/kde-logo.png
        
      - title: Trade Shows and Community Events
        heading: Supported Activities &#8210; Trade Shows and Community Events
        items:
          - include: 02_fosdem.html
            heading: FOSDEM
            author: By Paul Brown
            image: images/logos/FOSDEMx47.png
          - include: 02_gitlab.html
            heading: GitLab Commit
            author: By Paul Brown
            image: images/logos/gitlabx47.png
          - include: 02_qtws.html
            heading: Qt World Summit
            author: By Kai Uwe
            image: images/logos/qt.png
          - include: 02_pycon_india.html
            author: By Piyush Aggarwal
            heading: PyCon India
            image: images/logos/pycon.png
          - include: 02_open_expo.md
            heading: OpenExpo Europe 2019
            author: Paul Brown
            image: images/logos/openexpo.png

  - title: Reports
    items:
      - title: Goals
        heading: Goals
        items:
          - heading: Goal ‒ It's All about the Apps
            author: By Jonathan Riddell
            include: 03_goal_allabouttheapps.html
            image: images/logos/goals.png
          - heading: Goal ‒ Wayland
            author: By David Edmundson, Aleix Pol and Méven Car
            image: images/logos/goals.png
            include: 03_goal_wayland.html 
          - heading: Goal ‒ Consistency
            author: By Niccolo Venerandi
            image: images/logos/goals.png
            include: 03_goal_consistency.html
    items:
      - title: Working Groups
        heading: Working Groups
        items:
          - heading: Sysadmin
            include: 04_sysadmin.html
            image: images/logos/kde-logo.png
            author: By Ben Cooksley
---

<h2>

        <section class="gray" id="tradeshows">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <h2>Reports</h2>
                    </div>
                </div>
                <div w3-include-html="03_goal_allabouttheapps.html" id="sysadmin-report"></div>
                <div w3-include-html="03_goal_wayland.html" id="sysadmin-report"></div>
                <div w3-include-html="03_goal_consistency.html" id="sysadmin-report"></div>
            </div>
        </section>
        <br id="dev-report"/>
        <section>
            <div class="container">
                <div class="heading wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
                    <div class="row">
                        <div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <h2>Community Highlights</h2>
                        </div>
                    </div>
                    <div w3-include-html="highlights.html"></div>
                </div>
            </div>
        </section>
        <div class="gray" w3-include-html="quotes.html"></div>
        <div w3-include-html="new-members.html"></div>
        <div w3-include-html="board.html"></div>
        <section>
            <div class="container">
                <div class="heading wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
                    <div class="row">
                        <div class="col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <h2>Partners</h2>
                        </div>
                    </div>
                    <div w3-include-html="partners.html"></div>
                </div>
            </div>
        </section>
        <section id="about-kdeev" class="gray">
            <div class="container">
                <div class="heading wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h2>About KDE e.V.</h2>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 wow fadeInDown text-center" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <p><a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a registered non-profit organization that represents the <a href="http://www.kde.org" target="_blank">KDE Community</a> in legal and financial matters. The KDE e.V.'s purpose is the promotion and distribution of free desktop software in terms of free software, and the program package "K Desktop Environment (KDE)" in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#twitter-->
        <!--
        <section id="contact">
            <div id="google-map" class="wow fadeIn" data-latitude="52.5296452" data-longitude="13.4082609" data-wow-duration="1000ms" data-wow-delay="400ms"></div>
        </section>
        -->
        <ul class="address hidden">
            <li><i class="fa fa-map-marker"></i> <span> Address:</span> Schönhauser Allee 6-7,10119 Berlin, Germany </li>
            <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
            <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
            <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
        </ul>
        <!--/#contact-->
        <footer id="footer">
            <div class="footer-top wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="container text-center">
                    <div class="row">
                        <div class="heading text-center col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <p>Report prepared by Paul Brown with the help of Lydia Pintscher, Albert Vaca, Dan Vratil, David Faure, Kevin Ottens, Volker Krause, Sandro Knauß, Jonathan Riddell, Thomas Pfeiffer, Andy Betts, Boudewijn Rempt, Devaja Shah, Ivana Isadora Devčić, Sandro Andrade, Nitish Chauhan, Albert Astals, Roman Gilg, Valorie Zimmerman, Kai Uwe Broulik, Ben Cooksley, Eike Hein, David Edmundson, Neofytos Kolokotronis, and the Promo Team at large.
                                <br/>
                            This report is published by KDE e.V., copyright 2019, and licensed under Creative Commons-BY-3.0 (creativecommons.org/licenses/).
                            </p>
                        </div>
                    </div>
                    <br/>
                    <div class="footer-logo">
                        <a href="https://ev.kde.org/" target="_blank">
                            <img class="img-responsive" src="../shared-assets/images/logo.png" alt=""/>
                        </a>
                    </div>
                    <div class="social-icons">
                        <ul>
                            <li>
                                <a class="envelope" href="mailto:kde-ev-board@kde.org">
                                    <i class="fa fa-envelope" title="Contact the KDE e.V. Board of Directors"></i>
                                </a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/kdecommunity" target="_blank">
                                    <i class="fa fa-twitter" title="The KDE Community &#8210; Twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="facebook" href="https://www.facebook.com/kde/" target="_blank">
                                    <i class="fa fa-facebook" title="The KDE Community &#8210; Facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="linkedin" href="https://dot.kde.org" target="_blank">
                                    <i class="fa fa-newspaper-o" title="dot.kde.org"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="heading text-center col-sm-12 myfooter" data-wow-duration="1000ms" data-wow-delay="300ms">
                            KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | <a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script>w3IncludeHTML();</script>
        <script type="text/javascript" src="../shared-assets/js/jquery.js"></script>
        <script type="text/javascript" src="../shared-assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../shared-assets/js/jquery.inview.min.js"></script>
        <script type="text/javascript" src="../shared-assets/js/wow.min.js"></script>
        <script type="text/javascript" src="../shared-assets/js/mousescroll.js"></script>
        <script type="text/javascript" src="../shared-assets/js/smoothscroll.js"></script>
        <script type="text/javascript" src="../shared-assets/js/jquery.countTo.js"></script>
        <script type="text/javascript" src="../shared-assets/js/lightbox.min.js"></script>
        <script type="text/javascript" src="../shared-assets/js/main.js"></script>

        <!-- Piwik -->
        <script type="text/javascript">
            var pkBaseURL = "https://stats.kde.org/";
            document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            piwik_action_name = 'KDE e.V. Homepage';
            piwik_idsite = 7;
            piwik_url = pkBaseURL + "piwik.php";
            piwik_log(piwik_action_name, piwik_idsite, piwik_url);
        </script>
        <script type="text/javascript" src="/media/javascripts/piwikbanner.js"></script>
        <object><noscript><p>Analytics <img src="https://stats.kde.org/piwik.php?idsite=7" style="border:0" alt=""/></p></noscript></object>
        <!-- End Piwik Tag -->
    </body>
</html>
