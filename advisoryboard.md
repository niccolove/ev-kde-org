---
title: "Advisory Board"
menu_active: Organization
layout: page
---

The KDE e.V. Advisory Board provides a space for organizations to support KDE e.V. by giving feedback on KDE's activities and decisions. Furthermore, it is a channel to facilitate communication between KDE and the organisations on the Advisory Board.

<h2>What can KDE e.V. Advisory Board offer to its members?</h2>

<ul>
    <li>Regular meetings to give input and discuss topics of interest and relevance. In regular calls, we will discuss KDE initiatives which could benefit from KDE e.V. Advisory Board's input and identify opportunities for collaboration.</li>
    <li>Access to maintainers and other key decision makers inside KDE. KDE e.V. Advisory Board members will hear about and give input on the latest technological innovations as well as social/policy endeavours inside KDE from the people who drive them.</li>
    <li>Dedicated contact person. They are there to answer questions and put the KDE e.V. Advisory Board members in contact with the right people inside KDE.</li>
    <li>Regular updates on important things going on inside KDE. We will provide the KDE e.V. Advisory Board with an overview of most important things inside KDE so that it can stay up to date.</li>
    <li>Meetings at Akademy. We will have special KDE e.V. Advisory Board meetings for networking and direct exchange of ideas on the future of KDE.</li>
    <li>Attendance at sprints. We are always happy to have KDE e.V. Advisory Board members joining our sprints in order to foster further collaboration with KDE contributors.</li>
</ul>

<h2>What do we expect the KDE e.V. Advisory Board members to do?</h2>

<ul>
    <li>Provide insights into the needs of various stakeholders.</li>
    <li>Ability to combine our efforts to have more impact.</li>
    <li>Get a more diverse view from outside of our organization on topics that are relevant to KDE.</li>
    <li>Benefit from other organizations' experience.</li>
    <li>Networking with other people and organizations.</li>
</ul>

<h2>Joining the Advisory Board</h2>

Organizations can join the Advisory board either by becoming a <a href="/supporting-members/">KDE e.V. Patron</a> or by an explicit invitation by the <a href="/corporate/board/">KDE e.V. Board</a>.

<h2>Current members</h2>
<ul>
<li>Blue Systems</li>
<li>Canonical</li>
<li>City of Munich</li>
<li>Debian</li>
<li>enioka Haute Couture</li>
<li>FOSS Nigeria</li>
<li>FSF</li>
<li>FSFE</li>
<li>OSI</li>
<li>Private Internet Access</li>
<li>SUSE</li>
<li>The Document Foundation</li>
<li>The Qt Company</li>
</ul>
