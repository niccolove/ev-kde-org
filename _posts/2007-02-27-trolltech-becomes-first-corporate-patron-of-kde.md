---
title: 'Trolltech becomes first Corporate Patron of KDE'
date: 2007-02-27 00:00:00 
layout: post
---

<a href="http://www.trolltech.com">Trolltech</a>, the Norwegian company behind
the Qt toolkit has become the second Patron of
KDE. Trolltech itself should not need an introduction, since they
have worked together with the KDE project since its inception ten years ago. Knut Irvin,
the community manager for Trolltech points out that "KDE does an excellent job of making
UNIX-based desktops easy to use. Trolltech gains from feedback, bug reports and
the spread of Qt through the success of KDE".

Being a <a href="http://ev.kde.org/supporting-members.php">Patron of KDE</a>
is an ideal way to both support the KDE project and become a more
active member of the KDE community. After the inaugural
<a href="http://dot.kde.org/1160932072/">membership of Mark Shuttleworth</a>,
Trolltech is the first corporate Patron of KDE.

<em>"Being a Patron of KDE should help KDE when organising developer gatherings and
organising the voluntary effort done by thousands of developers"</em>, Irvin
continues. Trolltech's involvement with KDE is widely-acknowledged, the company
providing the Qt toolkit, a high performance, cross-platform developer framework
which is distributed as Free Software under the terms of the GPL license.

Patrons of KDE may display the "Patron of KDE" logo on their website and any
other material for as long as they are a Patron of KDE and will be listed on the
KDE e.V. website, if they so wish. This is the highest level of membership
available within KDE e.V., and will allow KDE e.V. to continue its work
supporting and maintaining the structures of development.

Of course, aside from financial matters, sponsors of KDE are a vital part of the
vibrant community outreach and relations scheme - feedback from all our
supporters helps to shape our shared goals and future development.

KDE e.V. wishes to thank all its current supporters, and would like to invite
all interested parties to help us continue to serve the KDE community.

