---
title: "Supporting Members"
layout: page
menu_active: Organization
description: Become a supporting member and help fund KDE activities
---

<p>If your company or organization would like to become a supporting member please
have a look at the <a href="/getinvolved/supporting-members/">information how
to become a supporting member of the KDE e.V.</a></p>

<p>Individuals wishing to support KDE financially should consider to <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=9">Join the Game</a></p>

<p>Currently the supporting members of the KDE e.V. are:</p>

<h2>Patrons of KDE
<img style="float:right;" src="/images/patron_small.png" class="clear" />
</h2>

<div class="img-patrons">
  <img src="https://kde.org/aether/media/patrons/canonical.svg" alt="Canonical" />
  <img src="https://kde.org/aether/media/patrons/google.svg" alt="Google" />
  <img src="https://kde.org/aether/media/patrons/suse.svg" alt="SUSE" />
  <img src="https://kde.org/aether/media/patrons/qt-company.svg" alt="Qt Company" />
  <img src="https://kde.org/aether/media/patrons/blue-systems.svg" alt="Blue Systems" />
  <img src="https://kde.org/aether/media/patrons/privateinternetaccess.svg" alt="Private Internet Access" />
  <img src="https://kde.org/aether/media/patrons/enioka-haute-couture-logo.svg" alt="Enioka Haute Couture" />
</div>

<h2>Supporters of KDE
<img style="float:right;" src="/images/supporter_small.png" class="clear" />
</h2>

<div class="img-patrons">
  <img src="/images/supportingmembers/kdab.png" alt="KDAB" />
  <img src="/images/supportingmembers/basyskom.png" alt="Basyskom" />
  <img src="/images/supportingmembers/kontent.png" alt="KONTENT GmbH" style="padding-left: 40px; padding-right: 40px;" />
</div>
